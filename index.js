/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function promptUserDetails () {
		let fullName = prompt('Enter your full name')
		let age = prompt('Enter your age')
		let location = prompt('Enter you location')
		console.log('Hello ' +fullName)
		console.log('You are ' +age+ ' years old')
		console.log('You live in ' +location)

	}

	promptUserDetails()

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	// function favoriteBands() {
	// 	let bands = ['The Beatles', 'Metallica', 'The Eagles', 'The Carpenters', 'Kiss']
		
	// 	for(let band = 0; band < 5; band++) {
	// 		let numbering = band + 1
	// 		console.log(numbering+'. '+bands[band])
	// 	}	
	// }
	// favoriteBands()

	function favoriteBands(bands, index) {
		if(index >= bands.length) return
		console.log(`${index + 1}. ${bands[index]}`);
		favoriteBands(bands, index + 1);
	}

	favoriteBands(['The Beatles', 'Metallica', 'The Eagles', 'The Carpenters', 'Kiss'], 0);

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favoriteMovies(movies, movieScore, index) {
		if(index >= movies.length) return
		console.log(`${index + 1}. ${movies[index]}`)
		console.log(`Rotten Tomatoes Rating: ${movieScore[index]}%`)
		favoriteMovies(movies, movieScore, index + 1)
	}

	favoriteMovies(['The Shining', 'Doctor Sleep', 'Heredetary', 'The Batman', 'The Dark Knight'], ['82','78', '90', '85', '94'], 0)

	// function favoriteMovies() {
	// 	let movie = ['The Shining', 'Doctor Sleep', 'Heredetary', 'The Batman', 'The Dark Knight']
	// 	let movieScore = ['82','78', '90', '85', '94']

	// 	for(let fav = 0; fav < 5; fav++) {
	// 		let numbering = fav + 1;
	// 		console.log( numbering + '. ' + movie[fav])
	// 		console.log('Rotten Tomatoes Rating: ' + movieScore[fav]+'%')
	// 	}
		
	// }
	// favoriteMovies()

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 

}


printFriends()